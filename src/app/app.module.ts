import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Http, HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import * as moment from 'moment/moment';

import { SignInPage } from '../pages/sign-in/sign-in.component';
import { RegExpService } from '../providers/regExp.service';
import { AlertConfigService } from '../providers/alertConfig.service';
import { ConfigService } from '../providers/config.service';
import { SessionService } from '../providers/session.service';
import { AuthService } from '../providers/auth.service';
import { NavigationPage } from '../pages/navigation/navigation.component';
import { NewsPage } from "../pages/news/news.component";
import { BirthdayWeekComponent } from '../components/birthday-week/birthday-week.component';
import { BirthdayWeekService } from '../providers/birthday-week.service';
import { MyHttp } from '../providers/http.service';
import { NewsService } from '../providers/news.service';
import { DetailPage } from '../pages/detail/detail.component';
import { MapComponent } from "../components/map/map.component";
import { MapService } from '../providers/map.service';
import { CreateNewsPage } from '../pages/create-news/create-news.component';
import { AdvertFormComponent } from '../components/advertForm/advertForm.component';
import { PollComponent } from '../components/poll/poll.component';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignInPage,
    NavigationPage,
    NewsPage,
    DetailPage,
    CreateNewsPage,
    BirthdayWeekComponent,
    MapComponent,
    AdvertFormComponent,
    PollComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignInPage,
    NavigationPage,
    NewsPage,
    DetailPage,
    CreateNewsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RegExpService,
    AlertConfigService,
    AuthService,
    SessionService,
    ConfigService,
    BirthdayWeekService,
    NewsService,
    MapService,
    {
      provide: Http,
      useClass: MyHttp,
      deps: [XHRBackend, RequestOptions, SessionService]
    },
  ]
})
export class AppModule {}
