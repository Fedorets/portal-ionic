export class Vote {
  public id: number;
  public value: string;
  public votes: number;

  constructor(data) {
    this.id = data.id;
    this.value = data.value;
    this.votes = data.votes;
  }
}
