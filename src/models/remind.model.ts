import moment from 'moment-with-locales-es6';
moment.locale('ru');

export class Remind {
  public id: number;
  public begin_dt: string;
  public end_dt: string;
  public exclude_recipients_ids: string;
  public hidden: boolean;
  public is_processed: boolean;
  public parent_message: number;
  public template: number;
  public icon: string;

  constructor(data) {
    this.id = data.id;
    this.exclude_recipients_ids = data.exclude_recipients_ids;
    this.hidden = data.hidden;
    this.is_processed = data.is_processed;
    this.parent_message = data.parent_message;
    this.template = data.template;
    this.begin_dt = moment(data.begin_dt).format('D MMMM YYYY h:mm');
    this.end_dt = moment(data.end).format('h:mm D MMM YYYY');
    this.icon = 'calendar';
  }
}

// begin_dt  :  "2017-09-14T06:00:00Z"
// end_dt  :  "2017-09-14T06:00:00Z"
// exclude_recipients_ids  :  ""
// hidden  :  false
// id  :  110
// is_processed  :  false
// parent_message  : 253
// template  :  1
