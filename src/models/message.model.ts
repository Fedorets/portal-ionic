import moment from 'moment-with-locales-es6';
moment.locale('ru');

import { Author } from './author.model';
import { Poll } from './poll.model';
import { Remind } from './remind.model';
import { Event } from './event.model';
import { Advert } from './advert.model';

export class Message {
  public author: Author;
  public comments_count: number;
  public content_object: Object;
  public content_type: number;
  public create_dt: string;
  public edit_dt: string;
  public id: number;
  public is_actual: boolean;
  public message_type: string;
  public object_id: number;
  public options: any[];
  public text: string;
  public title: string;

  constructor(data) {
    this.author = (data.author !== null) ? new Author(data.author) : null;
    this.comments_count = data.comments_count;
    this.content_type = data.content_type;
    this.create_dt = moment(data.create_dt).format('h:mm D MMM YYYY');
    this.edit_dt = moment(data.edit_dt).format('h:mm D MMM YYYY');
    this.id = data.id;
    this.is_actual = data.is_actual;
    this.message_type = data.message_type;
    this.object_id = data.object_id;
    this.options = data.options;
    this.text = data.text;
    this.title = data.title;

    switch(data.message_type) {
      case 'event':
        this.content_object = new Event(data.content_object);
        break;
      case 'poll':
        this.content_object = new Poll(data.content_object);
        break;
      case 'remind':
        this.content_object = new Remind(data.content_object);
        break;
      case 'advert':
        this.content_object = new Advert(data.content_object);
        break;
      default: break;
    }
  }
}

// Напоминание:
// begin_dt  :  "2017-09-14T06:00:00Z"
// end_dt  :  "2017-09-14T06:00:00Z"
// exclude_recipients_ids  :  ""
// hidden  :  false
// id  :  110
// is_processed  :  false
// parent_message  : 253
// template  :  1

// Объявление:
// id  :  108
// is_important  :  false
// relevance_dt  :  null

// Событие:
// date_time    :    "2017-10-06T14:10:00Z"
// id  :  27
// location  :  "Козак Палац, малый зал"
// location_url  :  "https://goo.gl/maps/H9JteotcPw"
// poll_end_date  :  "2017-10-04"

//Голосование
// id  :  20
// poll_end_date  :  "2017-08-02"
// poll_type  :  "public"

