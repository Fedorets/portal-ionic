import { Message } from './message.model';

export class News {
  public id: number;
  public user: number;
  public message: Message;
  public my_vote: string;
  public is_favourite: boolean;
  public is_readed: boolean;
  public read_dt: boolean;

  constructor(data) {
    this.id = data.id;
    this.user = data.user;
    this.message = new Message(data.message);
    this.my_vote = data.my_vote;
    this.is_favourite = data.is_favourite;
    this.is_readed = data.is_readed;
    this.read_dt = data.read_dt;
  }
}
