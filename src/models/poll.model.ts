import moment from 'moment-with-locales-es6';
moment.locale('ru');

export class Poll {
  public id: number;
  public poll_type: string;
  public poll_end_date: string;
  public icon: string;

  constructor(data) {
    this.id = data.id;
    this.poll_type = data.poll_type;
    this.poll_end_date = moment(data.poll_end_date).format('h:mm D MMMM YYYY');
    this.icon = 'calendar';
  }
}
// id  :  20
// poll_end_date  :  "2017-08-02"
// poll_type  :  "public"

