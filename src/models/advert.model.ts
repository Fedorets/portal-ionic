export class Advert {
  public id: number;
  public is_important: boolean;
  public relevance_dt: string;
  public icon: string;


  constructor(data) {
    this.id = data.id;
    this.is_important = data.is_important;
    this.relevance_dt = data.relevance_dt;
    this.icon = 'information-circle';
  }
}

// id  :  108
// is_important  :  false
// relevance_dt  :  null
