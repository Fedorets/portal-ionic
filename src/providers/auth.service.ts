import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'

import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { Login } from '../models/login.model';
import { User } from '../models/user.model';


@Injectable()
export class AuthService {
  constructor(private configService: ConfigService,
              private session: SessionService,
              private http: Http) {}

  /**
   * For login authorization and saving token && user in the session
   * @param data - object consist of email and password
   * @returns object of User
   */
  public signIn(data): any {
    return this.http.post(this.configService.loginPath, new Login(data)).map((res) => {
      let data = res.json().user;
      let user = new User(data);
      this.session.token = data.token;
      this.session.user = user;
      // this.isSignIn();
      return user;
    })
  }

  /**
   * @returns checking if session has token
   */
  public isLogin() {
    return this.session.token !== null;
  }

  /**
   * For remove data user from session (token and localStorage)
   */
  public logout() {
    return this.http.post(this.configService.logoutPath, '').map((res) => {
      this.session.removeToken();
      this.session.removeUser();
      return res.json();
    })
  }

  /**
   * for subscription, is user autorized
   */
  // public isListenAuthorization(): Observable<any> {
  //   return this.subject.asObservable();
  // }

  /**
   * Used on header (user-panel)
   */
  // public isSignIn() {
  //   if (this.session.token) {
  //     return this.subject.next(true);
  //   } else {
  //     this.session.removeUser();
  //     return this.subject.next(false);
  //   }
  // }

  /**
   * @returns object User from localStorage
   */
  public getUserData() {
    return new User(this.session.user);
  }
}
