import { Http, Request, RequestOptionsArgs, Response, RequestOptions, ConnectionBackend } from '@angular/http';
import { Observable } from 'rxjs';
import { Injectable, Injector } from '@angular/core';
import { SessionService } from './session.service';

@Injectable()
export class MyHttp extends Http {
  constructor(_backend: ConnectionBackend, _defaultOptions: RequestOptions, private session: SessionService) {
    super(_backend, _defaultOptions);
  }

  /**
   * For add headers to all request http
   */
  public request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    if (this.session.token) {
        if (options) {
            // options.headers.append('Access-Control-Allow-Origin', '172.16.100.145:8100/');
            options.headers.set('Authorization', `Token ${ this.session.token}`);
        } else {
            (<Request> url).headers.set('Authorization', `Token ${ this.session.token }`);
        }
    }
    return super.request(url, options);
  }
}
