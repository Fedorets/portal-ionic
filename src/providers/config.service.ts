export class ConfigService {
  public static imagePath: string = 'http://fe-kurs.light-it.loc:80';
  public static imageDefaultPath: string = './assets/img/default-avatar.png';
  public googleMapKey = 'AIzaSyBmhVhypTwikmmtoKVBpuyviUzNQfYXn0k';
  public googleSearchPath = "https://maps.googleapis.com/maps/api/place/textsearch/json?";
  public basePath: string = 'https://portal-dev.light-it.loc/api/';
  public loginPath: string = `${this.basePath}auth/login/`;
  public birthWeekPath: string = `${this.basePath}week_birthdays/`;
  public userMessagesPath: string = `${this.basePath}user_messages/`;
  public messagesPath: string = `${this.basePath}messages/`;
  public logoutPath: string = `${this.basePath}auth/logout/`;

}
