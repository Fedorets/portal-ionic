import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
  constructor() {
    console.log(localStorage.getItem('token'));
  }
  /**
   * Get token from cookie, used in guards, for checking if the user is logged in
   * @returns token
   */
  public get token() {
    return JSON.parse(localStorage.getItem('token'));
  }

  /**
   * Put token to the cookie, used in signIn function
   * @param data - token
   */
  public set token(data) {
    localStorage.setItem('token', JSON.stringify(data));
  }

  /**
   * Used for retrieve info about the user from localStorage
   * @returns object User or null
   */
  public get user() {
    return JSON.parse(localStorage.getItem('user'));
  }

  /**
   * Used to record info about user in the localStorage
   * @param data - object User
   */
  public set user(data) {
    localStorage.setItem('user', JSON.stringify(data));
  }

  /**
   * Delete user, token from localStorage
   */
  public removeUser() {
    localStorage.removeItem('user');
  }
  public removeToken() {
    localStorage.removeItem('token');
  }
}
