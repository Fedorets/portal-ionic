import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { ConfigService } from './config.service';
import { News } from '../models/news.model';

@Injectable()
export class NewsService {
  constructor(private configService: ConfigService,
              private http: Http) {}

  public getNews(filters, ...page) {
    let url = this.configService.userMessagesPath;
    if (page.length > 0) {
      url = `${url}?page=${page[0]}`;
    }
    if (filters.length > 0) {
      filters.forEach((i, index) => {
        if (page.length > 0) {
          url = url.concat(`&type=${i}`);
        } else {
          url = index === 0 ? url.concat(`?type=${i}`) : url.concat(`&type=${i}`);
        }
      });
    }
    // console.log(url);
    return this.http.get(url).map((res) => {
      let tmp = [];
      let data = res.json().results;
      console.log(data);
      data.forEach((message) => {
        tmp.push(new News(message));
      });
      return tmp;
    });
  }

  public createNews(data) {
    return this.http.post(this.configService.messagesPath, data).map((res) => {
      return res.json();
    });
  }
}
