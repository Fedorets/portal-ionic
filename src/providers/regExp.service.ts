import { Injectable } from '@angular/core';

@Injectable()
export class RegExpService {
  public email = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
  public password = /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[a-z]).*$/;
}
