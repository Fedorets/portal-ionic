import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { Http } from '@angular/http';

@Injectable()
export class MapService {
  constructor(private configService: ConfigService,
              private http: Http) {}
  public getMapLocation(location) {
    console.log('Location:',  location);
    return this.http.get(`${this.configService.googleSearchPath}query=${location}&key=${this.configService.googleMapKey}`).map((res) => {
      return res;
    });
  }
}
