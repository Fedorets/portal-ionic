import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { ConfigService } from './config.service';
import { User } from '../models/user.model';

@Injectable()
export class BirthdayWeekService {
  constructor(private configService: ConfigService,
              private http: Http) {}
  public getBirthdayWeek() {
    return this.http.get(this.configService.birthWeekPath).map((res) => {
      let arr = [];
      for(let user of res.json()) {
        arr.push(new User(user));
      }
      return arr;
    });
  }
}
