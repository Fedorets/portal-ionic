import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertConfigService {
  public alertResult: any;
  constructor(public alertCtrl: AlertController) {}

  public initializeAlert(title, subtitle) {
    this.alertResult = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    this.alertResult.present();
  }
}
