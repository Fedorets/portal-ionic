import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';

import { RegExpService } from '../../providers/regExp.service';
import { AlertConfigService } from '../../providers/alertConfig.service';
import { AuthService } from '../../providers/auth.service';
import { NavigationPage } from '../navigation/navigation.component';

@Component({
  selector: 'lt-sign-in',
  templateUrl: './sign-in.component.html'
})

export class SignInPage {
  public signInForm: FormGroup;

  constructor(private fb: FormBuilder,
              private alertService: AlertConfigService,
              private authService: AuthService,
              private navCtrl: NavController,
              private regExp: RegExpService) {
    this.signInForm = fb.group({
      email: ['elenafedorets65+1@gmail.com', [Validators.required]],
      password: ['qwerty123', [Validators.required]]
    })
    // Validators.pattern(regExp.email)]
  }

  public submit(e, form) {
    e.preventDefault();
    if (form.valid) {
      this.authService.signIn(form.value).subscribe(() => {
        this.navCtrl.setRoot(NavigationPage);
      });
    } else {
      this.alertService.initializeAlert('Стопе', 'Проверьте введенную Вами информацию.')
    }
  }
}
