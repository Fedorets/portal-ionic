import { Component, Input } from '@angular/core';
import { NavParams } from 'ionic-angular';

import { News } from '../../models/news.model';

@Component({
  selector: 'lt-detail',
  templateUrl: './detail.component.html'
})

export class DetailPage {
  @Input() public news: News;
  constructor(public navParams: NavParams) {
    this.news = this.navParams.get('news');
  }
}
