import { Component, ViewChild } from '@angular/core';
import { ModalController, Nav } from 'ionic-angular';

import { NewsPage } from '../news/news.component';
import { User } from '../../models/user.model';
import { AuthService } from '../../providers/auth.service';
import { SignInPage } from '../sign-in/sign-in.component';

@Component({
  selector: 'lt-navigation',
  templateUrl: './navigation.component.html'
})

export class NavigationPage {
  public user: User;
  public rootPage: any = NewsPage;
  @ViewChild(Nav) nav: Nav;
  public pages: Array<{title: string, icon: string, component: any}>;

  constructor(public modalCtrl: ModalController,
              private authService: AuthService) {
    this.pages = [
      { title: 'Лента', icon: 'calendar', component: NewsPage },
      { title: 'Сотрудники', icon: 'people', component: NewsPage },
      { title: 'Дни рождения', icon: 'flower', component: NewsPage },
      { title: 'Жизнь Light IT', icon: 'images', component: NewsPage },
      { title: 'Отпуска', icon: 'alarm', component: NewsPage },
      { title: 'Письмо доверия', icon: 'mail', component: NewsPage },
      { title: 'О Компании', icon: 'information-circle', component: NewsPage }
    ];
  }

  public openPage(page) {
    this.nav.setRoot(page.component);
  }

  public logout() {
    this.authService.logout().subscribe(() => {
      this.nav.setRoot(SignInPage);
    });
  }

  // public ionViewDidLoad() {
  //   if (this.isLogin) {
  //     this.user = this.authService.getUserData();
  //     console.log(this.user);
  //   }
  // }

  public ionViewWillLoad() {
    this.user = this.authService.getUserData();
  }
}
