import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { NewsService } from '../../providers/news.service';
import { Message } from '../../models/message.model';
import { DetailPage } from '../detail/detail.component';
import { CreateNewsPage } from '../create-news/create-news.component';


@Component({
  selector: 'lt-news',
  templateUrl: './news.component.html'
})

export class NewsPage {
  public news: Message[];
  public page: number;
  public activeFilters: any;
  constructor(private newsService: NewsService,
              public navCtrl: NavController,
              private alertCtrl: AlertController) {
    this.page = 1;
    this.activeFilters = [];
  }

  public uploadMore(infiniteScroll) {
    setTimeout(() => {
      this.newsService.getNews(this.activeFilters, this.page).subscribe((res) => {
        this.news = [...this.news, ...res];
        this.page++;
      });
      infiniteScroll.complete();
    }, 500);
  }

  public goToDetail(news) {
    this.navCtrl.push(DetailPage, { news: news });
  }

  public createNews() {
    this.navCtrl.push(CreateNewsPage);
  }

  public showFilters() {
    this.activeFilters.forEach((i) => {

    });
    let filtersAlert = this.alertCtrl.create({
      title: 'Фильтры',
      buttons: [
        {
          text: 'Отменить',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Ок',
          handler: (data) => {
            this.activeFilters = [...data];
            this.newsService.getNews(this.activeFilters).subscribe((res) => {
              this.news = res;
              this.page = 2;
            });
          }
        }
      ],
      inputs: [
        {
          label: 'Объявления',
          type: 'checkbox',
          value: 'advert',
          checked: this.isFilterActive('advert')
        },
        {
          label: 'Опросы',
          type: 'checkbox',
          value: 'poll',
          checked: this.isFilterActive('poll')
        },
        {
          label: 'События',
          type: 'checkbox',
          value: 'event',
          checked: this.isFilterActive('event')
        }
      ]
    });
    filtersAlert.present();
  }

  public ionViewWillEnter() {
     this.newsService.getNews([]).subscribe((res) => {
       this.news = res;
       this.page++;
     });
  }

  private isFilterActive(value) {
    let result = this.activeFilters.find((i) => i === value);
    return !!result;
  }
}
