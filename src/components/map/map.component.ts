import { Component, Input, OnInit } from '@angular/core';

@Component({
 selector: 'lt-map',
 templateUrl:'./map.component.html'
})

export class MapComponent implements OnInit {
  @Input() public location: any;
  constructor() {}

  public ngOnInit() {
    let map, marker, data, service;
    map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(0, 0),
      zoom: 15
    });
    service = new google.maps.places.PlacesService(map);
    service.textSearch({query: this.location}, (res) => {
      data = res[0].geometry.location;
      map.setCenter(new google.maps.LatLng(data.lat(), data.lng()));
      marker = new google.maps.Marker({
        position: {lat: data.lat(), lng: data.lng()},
        map: map
      });
      marker.setMap(map);
    });
  }
}
