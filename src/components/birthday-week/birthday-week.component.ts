import { Component, OnInit } from '@angular/core';

import { BirthdayWeekService } from '../../providers/birthday-week.service';

@Component({
  selector: 'lt-birth-week',
  templateUrl: './birthday-week.component.html'
})

export class BirthdayWeekComponent implements OnInit {
  public birthdays: any[];
  constructor(private birthService: BirthdayWeekService) {}

  public ngOnInit() {
    this.birthService.getBirthdayWeek().subscribe((res: any[]) => {
      this.birthdays = res;
    });
  }
}
