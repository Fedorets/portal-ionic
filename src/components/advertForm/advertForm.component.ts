import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NavController } from 'ionic-angular';

import { AlertConfigService } from '../../providers/alertConfig.service';
import { NewsService } from '../../providers/news.service';

@Component({
  selector: 'lt-advert-form',
  templateUrl: './advertForm.component.html'
})

export class AdvertFormComponent {
  public advertForm: FormGroup;
  constructor(public navCtrl: NavController,
              private fb: FormBuilder,
              private alertConfig: AlertConfigService,
              private newsService: NewsService) {
    this.advertForm = this.fb.group({
      message_type: ['advert'],
      text: [null, [Validators.maxLength(3000)]],
      title: [null, [Validators.required, Validators.maxLength(60)]],
      is_important: [false]
    });
    this.advertForm.get('is_important').valueChanges.subscribe((state) => {
      (state) ? this.advertForm.addControl('relevance_dt', new FormControl(null, [Validators.required])) : this.advertForm.removeControl('relevance_dt');
    });
  }
  public submit(e, form) {
    e.preventDefault();
    if (form.valid) {
      if (form.get('relevance_dt')) {
        form.patchValue({'relevance_dt': moment(form.get('relevance_dt').value).format()});
      }
      console.log(form);
      this.newsService.createNews(form.value).subscribe(() => {
        this.navCtrl.pop();
      });
    } else {
      this.alertConfig.initializeAlert('Внимание', 'Проверьте, введенную вами информацию.');
    }
  }
}
